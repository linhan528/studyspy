﻿
using Microsoft.EntityFrameworkCore;

namespace StudySpy.Service.FileAPI.DbContexts
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options): base (options)
        {

        }

        public DbSet<Models.File> Files { get; set; }
    }
}

