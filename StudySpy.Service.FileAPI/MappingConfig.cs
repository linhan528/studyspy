﻿
using AutoMapper;
using StudySpy.Service.FileAPI.Models;

namespace StudySpy.Service.FileAPI;
public class MappingConfig
{
    public static MapperConfiguration RegisterMaps()
    {
        var mappingConfig = new MapperConfiguration(config=> {
            config.CreateMap<FileDto, Models.File>();
            config.CreateMap<Models.File,FileDto>();
        });
        return mappingConfig;
    }

}
