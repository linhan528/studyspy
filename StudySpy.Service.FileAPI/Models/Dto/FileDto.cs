﻿
namespace StudySpy.Service.FileAPI.Models;
public class FileDto
{
    public int Id { get; set; }
    public string Name { get; set; }
    public string Link { get; set; }
}
