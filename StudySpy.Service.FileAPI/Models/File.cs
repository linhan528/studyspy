﻿
using System.ComponentModel.DataAnnotations;

namespace StudySpy.Service.FileAPI.Models;
public class File
{
    [Key]
    public int Id { get; set; }
    [Required]
    public string Name { get; set; }
    [Required]
    public string Link { get; set; }
}
