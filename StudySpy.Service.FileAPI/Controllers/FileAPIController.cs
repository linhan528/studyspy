﻿using Microsoft.AspNetCore.Mvc;
using StudySpy.Service.FileAPI.Models;
using StudySpy.Service.FileAPI.Models.Dto;
using StudySpy.Service.FileAPI.Repository;

namespace StudySpy.Service.FileAPI.Controllers;
[Route("api/files")]
public class FileAPIController : ControllerBase
{
    protected ResponseDto _response;
    private IFileRepository _fileRepository;

    public FileAPIController(IFileRepository fileRepository)
    {
        _fileRepository = fileRepository;
        this._response = new ResponseDto();
    }

   [HttpGet]
   public async Task<object> Get()
    {
        try
        {
            IEnumerable<FileDto> files = await _fileRepository.GetFiles();
            _response.Result = files;
        }
        catch (Exception ex)
        {

            _response.IsSuccess = false;
            _response.ErrorMessages = new List<string>() { ex.ToString() };
        }

        return _response;
    }

    [HttpGet]
    [Route("{id}")]
    public async Task<object> Get(int id)
    {
        try
        {
            FileDto file = await _fileRepository.GetFileById(id);
            _response.Result = file;
        }
        catch (Exception ex)
        {

            _response.IsSuccess = false;
            _response.ErrorMessages = new List<string>() { ex.ToString() };
        }

        return _response;
    }

    [HttpPost]
    public async Task<object> Post([FromBody] FileDto fileDto)
    {
        try
        {
            FileDto model = await _fileRepository.CreateUpdateFile(fileDto);
            _response.Result = model;
        }
        catch (Exception ex)
        {

            _response.IsSuccess = false;
            _response.ErrorMessages = new List<string>() { ex.ToString() };
        }

        return _response;
    }

    [HttpPut]
    public async Task<object> Put([FromBody] FileDto fileDto)
    {
        try
        {
            FileDto model = await _fileRepository.CreateUpdateFile(fileDto);
            _response.Result = model;
        }
        catch (Exception ex)
        {

            _response.IsSuccess = false;
            _response.ErrorMessages = new List<string>() { ex.ToString() };
        }

        return _response;
    }

    [HttpDelete]
    public async Task<object> Delete(int id)
    {
        try
        {
            bool isSuccess = await _fileRepository.DeleteFile(id);
            _response.Result = isSuccess;
        }
        catch (Exception ex)
        {

            _response.IsSuccess = false;
            _response.ErrorMessages = new List<string>() { ex.ToString() };
        }

        return _response;
    }
}
