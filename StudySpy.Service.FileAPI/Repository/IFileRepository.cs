﻿
using StudySpy.Service.FileAPI.Models;

namespace StudySpy.Service.FileAPI.Repository;
public interface IFileRepository
{
    Task<IEnumerable<FileDto>> GetFiles();
    Task<FileDto> GetFileById(int fileId);
    Task<FileDto> CreateUpdateFile(FileDto fileDto);
    Task<bool> DeleteFile(int fileId);
}
