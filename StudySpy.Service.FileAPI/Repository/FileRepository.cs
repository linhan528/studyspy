﻿
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using StudySpy.Service.FileAPI.DbContexts;
using StudySpy.Service.FileAPI.Models;

namespace StudySpy.Service.FileAPI.Repository;
public class FileRepository : IFileRepository
{
    private readonly ApplicationDbContext _db;

    private IMapper _mapper;

    public FileRepository(ApplicationDbContext db, IMapper mapper)
    {
        _db = db;
        _mapper = mapper;
    }
    public async Task<FileDto> CreateUpdateFile(FileDto fileDto)
    {
        Models.File file = _mapper.Map<FileDto, Models.File>(fileDto);

        if(file.Id>0)
        {
            _db.Files.Update(file);
        } else
        {
            _db.Files.Add(file);
        }

        await _db.SaveChangesAsync();

        return _mapper.Map<Models.File, FileDto>(file);

    }

    public async Task<bool> DeleteFile(int fileId)
    {
        try
        {
            Models.File file = await _db.Files.FirstOrDefaultAsync(x=>x.Id == fileId);
            if(file == null)
            {
                return false;
            }
            _db.Files.Remove(file);
            await _db.SaveChangesAsync();
            return true;
        } catch(Exception)
        {
            return false;
        }
    }

    public async Task<FileDto> GetFileById(int fileId)
    {
        Models.File file = await _db.Files.Where(x=>x.Id == fileId).FirstOrDefaultAsync();
        return _mapper.Map<FileDto>(file);
    }

    public async Task<IEnumerable<FileDto>> GetFiles()
    {
        List<Models.File> fileList = await _db.Files.ToListAsync();
        return _mapper.Map<List<FileDto>>(fileList);
    }
}
