﻿
using static StudySpy.Web.SD;

namespace StudySpy.Web.Models;
public class ApiRequest
{
    public ApiType ApiType { get;set; }
    public string Url { get; set; }
    public object Data { get; set; }
    public string AccessToken { get; set; }
}
