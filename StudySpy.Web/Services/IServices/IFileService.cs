﻿
using StudySpy.Web.Models;

namespace StudySpy.Web.Services.IServices;
public interface IFileService: IBaseService
{
    Task<T> GetAllFilesAsync<T>();
    Task<T> GetFileByIdAsync<T>(int id);
    Task<T> CreateFileAsync<T>(FileDto fileDto);
    Task<T> UpdateFileAsync<T>(FileDto fileDto);
    Task<T> DeleteFileAsync<T>(int id);
}
