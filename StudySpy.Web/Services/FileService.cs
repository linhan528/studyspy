﻿
using StudySpy.Web.Models;
using StudySpy.Web.Services.IServices;

namespace StudySpy.Web.Services;
public class FileService : BaseService, IFileService
{
    private readonly IHttpClientFactory _clientFactory;
    public FileService(IHttpClientFactory clientFactory): base(clientFactory)
    {
        _clientFactory = clientFactory;
    }
    public async Task<T> CreateFileAsync<T>(FileDto fileDto)
    {
        return await this.SendAsync<T>(new ApiRequest()
        {
            ApiType = SD.ApiType.POST,
            Data = fileDto,
            Url = SD.FileAPIBase + "/api/files",
            AccessToken = ""
        });
    }

    public async Task<T> DeleteFileAsync<T>(int id)
    {
        return await this.SendAsync<T>(new ApiRequest()
        {
            ApiType = SD.ApiType.DELETE,
            Url = SD.FileAPIBase + "/api/files/" + id,
            AccessToken = ""
        });
    }

    public async Task<T> GetAllFilesAsync<T>()
    {
        return await this.SendAsync<T>(new ApiRequest()
        {
            ApiType = SD.ApiType.GET,
            Url = SD.FileAPIBase + "/api/files",
            AccessToken = ""
        });
    }

    public async Task<T> GetFileByIdAsync<T>(int id)
    {
        return await this.SendAsync<T>(new ApiRequest()
        {
            ApiType = SD.ApiType.GET,
            Url = SD.FileAPIBase + "/api/files/" + id,
            AccessToken = ""
        });
    }

    public async Task<T> UpdateFileAsync<T>(FileDto fileDto)
    {
        return await this.SendAsync<T>(new ApiRequest()
        {
            ApiType = SD.ApiType.PUT,
            Data = fileDto,
            Url = SD.FileAPIBase + "/api/files",
            AccessToken = ""
        });
    }
}
