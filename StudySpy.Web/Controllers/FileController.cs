﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using StudySpy.Web.Models;
using StudySpy.Web.Services.IServices;

namespace StudySpy.Web.Controllers;
public class FileController : Controller
{
    private readonly IFileService _fileService;
    public FileController(IFileService fileService)
    {
        _fileService = fileService;
    }

    public async Task<IActionResult> FileIndex()
    {
        List<FileDto> list = new();
        var response = await _fileService.GetAllFilesAsync<ResponseDto>();
        if (response != null && response.IsSuccess)
        {
            list = JsonConvert.DeserializeObject<List<FileDto>>(Convert.ToString(response.Result));
        }
        return View(list);
    }

    public async Task<IActionResult> FileCreate()
    {
        return View(new FileDto());
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> FileCreate(FileDto model)
    {

        if (model.File !=null && model.File.Length > 512000)
        {
            ModelState.AddModelError("File", "The file is over 500kb");
        }

        if (ModelState.IsValid)
        {
            string fileName = string.Format("{0}_{1}", DateTime.Now.ToString("yyyyMMddHHmmssffff"), model.File.FileName);
            using (Stream stream = new FileStream(Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "files", fileName), FileMode.Create))
            {
                await model.File.CopyToAsync(stream);
            }
            model.Link = string.Format("/{0}/{1}", "files", fileName);
            var response = await _fileService.CreateFileAsync<ResponseDto>(model);
            if (response != null && response.IsSuccess)
            {
                return RedirectToAction(nameof(FileIndex));
            }
        }
        return View(model);
    }

    public async Task<IActionResult> FileDelete(int fileId)
    {
        var response = await _fileService.GetFileByIdAsync<ResponseDto>(fileId);
        if (response != null && response.IsSuccess)
        {
            FileDto model = JsonConvert.DeserializeObject<FileDto>(Convert.ToString(response.Result));
            return View(model);
        }
        return NotFound();
    }
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> FileDelete(FileDto model)
    {
        if (ModelState.IsValid)
        {
            var response = await _fileService.DeleteFileAsync<ResponseDto>(model.Id);
            if (response.IsSuccess)
            {
                return RedirectToAction(nameof(FileIndex));
            }
        }
        return View(model);
    }
}

